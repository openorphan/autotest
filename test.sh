#!/bin/bash
set -x
touch /tmp/testfile
yum install -y git
rpm -ivh "https://packages.chef.io/stable/el/7/chef-12.13.37-1.el7.x86_64.rpm"
cd /root
git clone "https://openorphan@bitbucket.org/openorphan/autotest.git"
chef-solo -c "/root/autotest/solo.rb" -j "/root/autotest/node.json"
