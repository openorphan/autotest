cookbook_file "/etc/yum.repos.d/cassandra.repo" do
 source "cassandra.repo"
 mode "0644"
 owner "root"
 group "root"
end

package "dsc20" do
 action :install
end

service "cassandra" do
 action [:enable,:start]
end

