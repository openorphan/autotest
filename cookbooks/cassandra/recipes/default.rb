#
# Cookbook Name:: cassandra
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe "oracle_java8"
include_recipe "cassandra::install_cassandra"
