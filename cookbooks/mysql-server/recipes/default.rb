#
# Cookbook Name:: mysql-server
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
cookbook_file '/etc/yum.repos.d/mysql-community-source.repo' do
 source 'mysql-community-source.repo'
 owner 'root'
 group 'root'
 mode '0644'
end

cookbook_file '/etc/yum.repos.d/mysql-community.repo' do
 source 'mysql-community.repo'
 owner 'root'
 group 'root'
 mode '0644'
end

cookbook_file '/etc/pki/rpm-gpg/RPM-GPG-KEY-mysql' do
 source 'RPM-GPG-KEY-mysql'
 owner 'root'
 group 'root'
 mode '0644'
end

mysql_service 'server' do
  port '3306'
  version '5.7'
  initial_root_password 'changeme'
  action [:create, :start]
end
