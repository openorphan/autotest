default["oracle"]["javaPackage"] = "jdk-8u60-linux-x64.rpm"
default["oracle"]["javaUrl"] = "http://download.oracle.com/otn-pub/java/jdk/8u60-b27"
default["oracle"]["java"] = "/usr/bin/java"
