#
# Cookbook Name:: oracle_java8
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
execute "Download java rpm" do
  command "wget --no-cookies --no-check-certificate --header 'Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie' -P /tmp #{node["oracle"]["javaUrl"]}/#{node["oracle"]["javaPackage"]}"
 not_if { File.exist?("/tmp/#{node["oracle"]["javaPackage"]}")}
end

#Install rpm packege download to /tmp dir

rpm_package "#{node["oracle"]["javaPackage"]}" do
 source  "/tmp/#{node["oracle"]["javaPackage"]}"
 action :install
end

